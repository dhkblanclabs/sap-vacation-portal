import { Route, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { InboxComponent } from './component/inbox/inbox.component';
import { RequestComponent } from './component/request/request.component';


const ROUTES: Route[] = [
    {
        path: '',
        component: AppComponent
    },
    {
        path: 'request/:id',
        component: RequestComponent
    },
    {
        path: 'inbox',
        component: InboxComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(ROUTES, { useHash: true })
    ],
    exports: [
        RouterModule,
    ]
})
export class RoutingModule {
}
