import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { SapModuleService } from '../../services/sap-module.service';
import { Request } from '../../model/request';
import { RequestStatus } from '../../model/RequestStatus';

@Component({
    selector: 'app-request',
    templateUrl: './request.component.html',
    styleUrls: ['./request.component.scss']
})
export class RequestComponent implements OnInit, OnDestroy {

    // Request identification
    private requestId: number;
    private sub: any;
    // Request fetched using the identificatoin
    request: Request;

    constructor(private route: ActivatedRoute, private sapModuleService: SapModuleService, private router: Router) { }

    // At the init of the get the request by the given identification
    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.requestId = +params.id;
            this.sapModuleService.getVacationRequest(this.requestId).subscribe(req => {
                console.log('Request data fetched successful ', req);
                this.request = req;
            }, error => {
                console.log('ERROR while requesting REQUEST ' + this.requestId, error);
                this.router.navigateByUrl('inbox');
            });
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    // Call update of the request with ACEPTED
    approveRequest() {
        this.updateRequest(RequestStatus.ACCEPTED);
    }

    // Call update of the request with REJECTED
    rejectRequest() {
        this.updateRequest(RequestStatus.REJECTED);
    }

    private updateRequest(status: RequestStatus) {
        this.sapModuleService.updateVacationRequest(this.request.id, status);
    }

    isRequestOpend() {
        return this.request.status === RequestStatus.OPEN;
    }

    // Call update of the request
    goHome() {
        this.router.navigateByUrl('inbox');
    }

    // Calculate requested days
    getRequestedDays() {
        const oneDay = 1000 * 60 * 60 * 24;
        const startDateMs = Date.parse(this.request.startDate.toString());
        const endDateMs = Date.parse(this.request.endDate.toString());
        const requestedDaysMs = endDateMs - startDateMs;
        return Math.round(requestedDaysMs / oneDay);
    }
}
