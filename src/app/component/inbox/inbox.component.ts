import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SapModuleService } from '../../services/sap-module.service';
import { Request } from '../../model/request';
import { RequestStatus } from '../../model/RequestStatus';

@Component({
    selector: 'app-inbox',
    templateUrl: './inbox.component.html',
    styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {

    // Fetched requests from server
    requests: Request[];
    // Service for getting infromation
    sapModuleService: SapModuleService;

    constructor(private service: SapModuleService, private router: Router) {
        this.requests = [];
        this.sapModuleService = service;
        this.getAllRequest();
    }

    // Call service on init
    ngOnInit() {
        this.getAllRequest();
    }

    // Call service for retrieve data
    getAllRequest() {
        this.sapModuleService.getAllVacationRequest().subscribe(data => {
            this.requests = data;
        });
    }

    // Method for start request edition
    editRequest(requestId: number) {
        this.router.navigateByUrl('/request/' + requestId);
    }

    // Request status validation
    isRequestOpen(req: Request) {
        return RequestStatus.OPEN === req.status;
    }
}
