import { RequestStatus } from '../model/RequestStatus';
// Request model
export interface Request {
    'id': number;
    'startDate': Date;
    'endDate': Date;
    'description': string;
    'employee': string;
    'employeeId': number;
    'title': string;
    'status': RequestStatus;
    'createdDate': Date;
}
