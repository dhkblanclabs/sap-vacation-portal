// Posible status of the request
export enum RequestStatus {
    OPEN = 'OPEN', ACCEPTED = 'ACCEPTED', REJECTED = 'REJECTED'
}
