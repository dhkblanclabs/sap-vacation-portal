import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { environment } from '../../environments/environment';

import { Request } from '../model/request';
import { RequestStatus } from '../model/RequestStatus';

@Injectable({
    providedIn: 'root'
})
export class SapModuleService {

    constructor(private httpClient: HttpClient, private router: Router) { }

    // Service for geting all the requests
    getAllVacationRequest() {
        return this.httpClient.get<Request[]>(environment.SAP_MODULE);
    }

    // Service for geting all the information of a request by give identification
    getVacationRequest(id: number) {
        return this.httpClient.get<Request>(environment.SAP_MODULE + '/' + id);
    }

    // Service for update status of a given request
    updateVacationRequest(id: number, newStatus: RequestStatus) {
        return this.httpClient.patch(environment.SAP_MODULE + '/' + id, newStatus).subscribe(data => {
            console.log('UPDATE Request is successful ');
            this.router.navigateByUrl('inbox');
        }, error => {
            console.log('ERROR while requesting UPDATE ' + id, error);
        }
        );
    }
}
