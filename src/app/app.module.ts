import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { NotifierModule } from 'angular-notifier';
import { RoutingModule } from './routing.module';

import { AppComponent } from './app.component';
import { SapModuleService } from './services/sap-module.service';
import { InboxComponent } from './component/inbox/inbox.component';
import { RequestComponent } from './component/request/request.component';


@NgModule({
    declarations: [
        AppComponent,
        InboxComponent,
        RequestComponent
    ],
    imports: [
        BrowserModule,
        RoutingModule,
        HttpClientModule,
        NotifierModule
    ],
    providers: [SapModuleService],
    bootstrap: [AppComponent]
})
export class AppModule { }
